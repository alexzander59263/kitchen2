import { VIEWPORTTAG } from './ts/models/settings';
import { Drawer } from './ts/models/drawer';
import { Layer } from './ts/models/layer';
export const App = () => {
    const [viewport] = document.getElementsByTagName(VIEWPORTTAG);
    viewport.style.height = viewport.clientWidth * 0.8 + 'px';
    const Kitchen = new Drawer(viewport);
    Kitchen.Fixed = new Layer('k1', viewport, [0, 0, 600, 150, 'grey']);
    Kitchen.Fixed = new Layer('k2', viewport, [0, 150, 150, 300, 'grey']);
    Kitchen.Zone = new Layer('denyzone1', viewport, [110, 0, 150, 150, 'red']);
    Kitchen.Zone = new Layer('coldzone', viewport);
    Kitchen.Zone = new Layer('hotzone', viewport, [300, 0, 150, 150, 'green']);
    /*  viewport.addEventListener('touch', (e: any) => {
         const [t] = e.touches
         console.log('touch', e);
         console.log('t3', t.clientX, t.clientY);
         Kitchen.getActive(t.clientX, t.clientX)
         //Kitchen.getActive(e.layerX, e.layerY)
     }) */
    viewport.addEventListener('click', (e) => {
        Kitchen.getActive(e.layerX, e.layerY);
        console.log('t4', e.layerX, e.layerY);
    }, false);
    viewport.addEventListener('touchmove', (e) => {
        const [t] = e.touches;
        console.log('t1', t.clientX, t.clientY);
        const active = Kitchen.Active;
        if (!active)
            return;
        const [x, y] = Kitchen.canPlace(t.clientX, t.clientY, active);
        active.Place(x, y);
    });
    /* viewport.addEventListener('mousemove', (e: any) => {

        console.log('t2', e.layerX, e.layerY);
        const active = Kitchen.Active
        if (!active) return
        const [x, y] = Kitchen.canPlace(e.layerX - 50, e.layerY - 50, active)

        active.Place(x, y)
    }, false) */
};
//# sourceMappingURL=app.js.map
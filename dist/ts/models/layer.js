export class Layer {
    constructor(name, viewport, opts = []) {
        this.viewport = null;
        this.Canvas = null;
        this.ctx = null;
        this.active = false;
        this.order = 0;
        this.padding = 10;
        const [x, y, w, h, color] = opts;
        this.Dims = [x || 0, y || 0, w || 100, h || 100];
        this.name = name;
        this.viewport = viewport;
        this.fillStyle = color || 'red';
        this._Canvas();
        this.Place();
    }
    Active() {
        this.active = true;
        this.Canvas.style.zIndex = 1;
        this.Canvas.classList.add('active');
    }
    Fix() {
        this.active = false;
        this.Canvas.style.zIndex = 0;
        this.Canvas.classList.remove('active');
    }
    _Canvas() {
        this.Canvas = document.createElement('canvas');
        this.Canvas.id = `layer_${this.name}`;
        this.Canvas.width = this.viewport.clientWidth;
        this.Canvas.height = this.viewport.clientHeight || 800;
        this.Canvas.style.zIndex = 0;
        this.viewport.append(this.Canvas);
        this.ctx = this.Canvas.getContext('2d');
    }
    set setDims([x, y]) {
        this.Dims[0] = x;
        this.Dims[1] = y;
    }
    Place(x = this.Dims[0], y = this.Dims[1]) {
        this.clear();
        this.ctx.fillStyle = this.fillStyle;
        this.setDims = [x, y];
        this.ctx.fillRect(...this.Dims);
        //  this.ctx.strokeRect(...this.Dims)
    }
    Bounds(x = this.Dims[0], y = this.Dims[1]) {
        const [x1, y1, w, h] = this.Dims;
        return [x, (x + w), y, (y + h)];
    }
    get Zones() {
        const [x, y, w, h] = this.Dims;
        return [
            [x - this.padding, x + this.padding, y - this.padding,]
        ];
    }
    Corners(x = this.Dims[0], y = this.Dims[1]) {
        const [x1, y1, w, h] = this.Dims;
        return [
            [x, y],
            [x + w, y],
            [x + w, y + h],
            [x, y + h],
        ];
    }
    clear() {
        this.ctx.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
    }
    get W() {
        return this.Dims[2];
    }
    get H() {
        return this.Dims[3];
    }
}
//# sourceMappingURL=layer.js.map
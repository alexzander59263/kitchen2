export class Drawer {
  constructor(viewport) {
    this.Zones = new Map();
    this.FixedZones = new Map();
    this.DenyZones = new Map();
    this.order = 0;
    this.viewport = viewport;
  }
  set Zone(z) {
    z.order = this.order;
    this.Zones.set(z.name, z);
    this.order++;
  }
  set Deny(z) {
    z.order = this.order;
    this.DenyZones.set(z.name, z);
    this.order++;
  }
  set Fixed(z) {
    z.order = this.order;
    this.FixedZones.set(z.name, z);
    this.order++;
  }
  getActive(layerX, layerY) {
    const active = this.OnElem(layerX, layerY);
    console.log("a", active);
    if (active) !active.active ? this.Activate(active) : active.Fix();
  }
  Activate(z) {
    this.Zones.forEach((z) => z.Fix());
    z.Active();
  }
  get Active() {
    return [...this.Zones.values()].find((z) => z.active);
  }
  OnElem(layerX, layerY) {
    return [...this.Zones.values()].find((z) => {
      const b = z.Bounds();
      return (
        layerX >= b[0] && layerX <= b[1] && layerY >= b[2] && layerY <= b[3]
      );
    });
  }
  inArea([x, y]) {
    return [...this.FixedZones.values()].find((z) => {
      const b = z.Bounds();
      return x >= b[0] && x <= b[1] && y >= b[2] && y <= b[3];
    });
  }
  canPlace(layerX, layerY, layer) {
    const Corners = layer.Corners(layerX, layerY);
    let Coords = [layerX, layerY];
    /*
                const inArea = Corners.map(c => this.inArea(c))
                if (!inArea[2] && !inArea[3]) Coords = [Coords[0],]
                console.log('inArea', inArea); */
    return Coords;
  }
  _canPlace(layerX, layerY, layer) {
    const bounds = layer.Bounds(layerX, layerY);
    let Coords = [layerX, layerY];
    /*   const z = [...([...this.Zones.values()].filter((z: Layer) => z != layer)), ...this.FixedZones.values()].map((z: Layer) => {
              const b = z.Bounds()
              return [b[0], b[1]]
          });
  
          console.log('z', [...new Set(z.flat())].sort());
  
          console.log('x', ([...new Set(z.flat())].sort()).sort((a, b) => { console.log('1', a, b); return a }
          )); */
    [...this.Zones.values()]
      .filter((z) => z != layer)
      .forEach((z) => {
        const b = z.Bounds();
        const xi1 = bounds[0] <= b[1] && bounds[0] >= b[0];
        const xi2 = bounds[1] >= b[0] && bounds[1] <= b[1];
        const yi1 = bounds[2] <= b[3] && bounds[2] >= b[2];
        const yi2 = bounds[3] >= b[2] && bounds[3] <= b[3];

        if (xi1 && (yi1 || yi2)) Coords = [b[1], Coords[1]];
        //if (xi1 && yi2) Coords = [b[1], Coords[1]];
        if (xi2 && (yi1 || yi2)) Coords = [b[0] - layer.W, Coords[1]];
        //if (yi1) Coords = [Coords[0], b[3]];
        // if (yi2) Coords = [Coords[0], (b[2] - layer.H)];
        //if (yi2) Coords = [(b[0] - layer.W), Coords[1]];
        /*   if (yi1 && yi2) { //Справа или Слева
                  if (xi1) {
                      Coords = [b[1], Coords[1]];
                  }
                  else if (xi2) {
                      Coords = [(b[0] - layer.W), Coords[1]];
                  }
              }
              else if (xi1 && xi2) { //Сверху или Снизу
                  if (yi1) {
                      Coords = [Coords[0], b[3]];
                  }
                  else if (yi2) {
                      Coords = [Coords[0], (b[2] - layer.H)];
                  }
              } else if (xi1 && yi1 || xi2 && yi1) {
                  Coords = [Coords[0], b[3]];
              } else if (xi2 && yi2 || xi1 && yi2) {
                  Coords = [Coords[0], (b[2] - layer.H)];
              } */
        /*  if (xi1 && yi1 || xi1 && yi2) { Coords = [b[1], layerY]; return }
             else if (xi2 && yi1 || xi2 && yi2) {
                 Coords = [(b[0] - layer.W), layerY]; return
             }
             else if (yi1 && xi1 || yi1 && xi2) {
                 Coords = [layerX, b[3]]; return
             } */
        /*  const i2 = b[1].filter(x => bounds[1].includes(x))
             console.log('x', i1.length);
             console.log('y', i2.length);
             return i1.length && i2.length */
      });
    this.FixedZones.forEach((z) => {
      const b = z.Bounds();
      if (bounds[0] < b[0]) Coords = [b[0], Coords[1]];
      if (bounds[1] > b[1]) Coords = [b[1] - layer.W, Coords[1]];
      if (bounds[2] < b[2]) Coords = [Coords[0], b[2]];
      if (bounds[3] > b[3]) Coords = [Coords[0], b[3] - layer.H];
      // const yi1 = bounds[2] <= b[3] && bounds[2] >= b[2]
      //const yi2 = bounds[3] >= b[2] && bounds[3] <= b[3]
    });
    //if (intersection.length) return []
    /*  const e = this.OnElem(layerX, layerY)
         if (!e) return true
         if (!e.active) return false */
    return Coords;
  }
}
//# sourceMappingURL=drawer.js.map

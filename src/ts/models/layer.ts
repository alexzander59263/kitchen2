export class Layer {
    viewport: any = null
    Canvas: any = null
    name: string
    ctx: any = null
    Dims: [number, number, number, number]
    fillStyle: string
    active: boolean = false
    order: number = 0
    padding: number = 10
    Cells: Set<any> = new Set()
    Step: number = 5
    Props: Array<string> = []
    constructor(name: string, viewport: Element, opts: Array<any> = []) {
        const [x, y, w, h, color] = opts
        this.Dims = [x || 0, y || 0, w || 100, h || 100]
        this.name = name
        this.viewport = viewport
        this.fillStyle = color || 'red'
        this._Canvas()
        this.Place()
    }
    Active() {
        this.active = true
        this.Canvas.style.zIndex = 1
        this.Canvas.classList.add('active')
        this.Place()
    }
    Fix() {
        this.active = false
        this.Canvas.style.zIndex = 0
        this.Canvas.classList.remove('active')
        this.Place()
        this.OuterGrid()
    }
    _Canvas() {
        this.Canvas = document.createElement('canvas')
        this.Canvas.id = `layer_${this.name}`
        this.Canvas.width = this.viewport.clientWidth
        this.Canvas.height = this.viewport.clientHeight || 800
        this.Canvas.style.zIndex = 0
        this.viewport.append(this.Canvas)
        this.ctx = this.Canvas.getContext('2d');
    }
    set setDims([x, y]: any) {
        this.Dims[0] = x
        this.Dims[1] = y
    }
    Place(x = this.Dims[0], y = this.Dims[1]) {
        this.clear()
        this.ctx.fillStyle = this.fillStyle
        this.setDims = [x, y]
        this.ctx.fillRect(...this.Dims)
        this.ctx.strokeStyle = 'black'
        this.ctx.lineWidth = 0.2
        this.ctx.strokeRect(...this.Dims)
        const [xx, yy, ww, hh] = this.Dims
        if (this.active) {
            this.ctx.shadowBlur = 15;
            this.ctx.strokeStyle = 'white'
            this.ctx.lineWidth = 5
            this.ctx.strokeRect(xx + 2.5, yy + 2.5, ww - 5, hh - 5)
        }
        this.Grid()
    }
    Bounds(x: number = this.Dims[0], y: number = this.Dims[1]) {
        const [x1, y1, w, h] = this.Dims
        return [x, (x + w), y, (y + h)]
    }
    Corners(x: number = this.Dims[0], y: number = this.Dims[1]) {
        const [x1, y1, w, h] = this.Dims
        return [
            [x, y, x + this.Step, y + this.Step],
            [(x + w) - this.Step, y, (x + w), y + this.Step],
            [(x + w) - this.Step, (y + h) - this.Step, (x + w), y + h],
            [x, (y + h) - this.Step, x + this.Step, y + h]
        ]
    }
    private clear() {
        this.ctx.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
    }
    get W() {
        return this.Dims[2]
    }
    get H() {
        return this.Dims[3]
    }
    get Coords() {
        return [this.Dims[0], this.Dims[1]]
    }
    get OuterDims() {
        const [x, y, w, h] = this.Dims
        return [
            (x - this.padding),
            (y - this.padding),
            (w + (2 * this.padding)),
            (h + (2 * this.padding))
        ]
    }

    Grid(d = this.Step, x1: number = this.Dims[0], y1: number = this.Dims[1]) {
        this.Cells.clear()
        for (let x = x1 / d; x < ((x1 + this.W) / d); x++) {
            for (let y = y1 / d; y < ((y1 + this.H) / d); y++) {
                this.Cells.add([x * d, y * d, (x * d) + d, (y * d) + d].join())
                //this.ctx.strokeRect(x * d, y * d, d, d)
            }
        }
    }
    OuterGrid(d = this.Step, x1: number = this.OuterDims[0], y1: number = this.OuterDims[1]) {
        const [x, y, W, H] = this.OuterDims
        this.Cells.clear()
        for (let x = x1 / d; x < ((x1 + W) / d); x++) {
            for (let y = y1 / d; y < ((y1 + H) / d); y++) {
                this.Cells.add([x * d, y * d, (x * d) + d, (y * d) + d].join())
                //this.ctx.strokeRect(x * d, y * d, d, d)
            }
        }
    }

}
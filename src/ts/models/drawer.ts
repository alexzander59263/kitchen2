import { Layer } from "./layer"
export class Drawer {
    viewport: Element
    Zones: Map<string, Layer> = new Map()
    FixedZones: Map<string, Layer> = new Map()
    DenyZones: Map<string, Layer> = new Map()
    order: number = 0
    Free: Array<any> = []
    Step: number = 5
    constructor(viewport: Element) {
        this.viewport = viewport
    }
    set Zone(z: Layer) {
        z.order = this.order
        this.Zones.set(z.name, z)
        this.order++
    }
    set Deny(z: Layer) {
        z.order = this.order
        this.DenyZones.set(z.name, z)
        this.order++
    }
    set Fixed(z: Layer) {
        z.order = this.order
        this.FixedZones.set(z.name, z)
        this.order++
    }
    fixAll() {
        this.Zones.forEach((z: Layer) => z.Fix())
    }
    getActive(layerX: number, layerY: number) {
        const active = this.OnElem(layerX, layerY)
        if (active) !active.active ? this.Activate(active) : active.Fix()
    }
    Activate(z: Layer) {
        this.fixAll()
        z.Active()
        this.Free = this.FreeCells(z)
    }
    get Active() {
        return [...this.Zones.values()].find((z: Layer) => z.active)
    }
    OnElem(layerX: number, layerY: number) {
        return [...this.Zones.values()]
            .find((z: Layer) => {
                const b = z.Bounds();
                return layerX >= b[0] && layerX <= b[1] && layerY >= b[2] && layerY <= b[3]
            })
    }
    getCell([x, y]: any) {
        const xm = Math.ceil(x / this.Step) * this.Step
        const ym = Math.ceil(y / this.Step) * this.Step
        return [xm - this.Step, ym - this.Step, xm, ym]
    }
    CheckCorners(x: number, y: number, layer: Layer) {
        const Corners = layer.Corners(x, y)
        return Corners.map(z => this.Free.includes(z.join()) ? 1 : 0).join('')
    }
    canPlace(layerX: number, layerY: number, layer: Layer) {
        let Coords = layer.Coords
        const p = this.getCell([layerX, layerY])
        let NewCoords = [p[0], p[1]]
        // console.log('d', d);
        const c = this.CheckCorners(p[0], p[1], layer)
        switch (c) {
            case '1111':
                Coords = NewCoords
                break
            case '1001':
                Coords = [(layer.Coords[0]), NewCoords[1]]
                break
            case '0110':
                Coords = [(layer.Coords[0]), NewCoords[1]]
                break
            case '1100':
                Coords = [NewCoords[0], layer.Coords[1]]
                break
            case '0011':
                Coords = [NewCoords[0], layer.Coords[1]]
                break
            case '0000':
            case '0010':
            case '0100':
            case '0001':
            case '1000':
            case '1101':
                Coords = layer.Coords
                break
        }
        layer.Grid(this.Step, ...Coords)
        const b = [...layer.Cells].filter(w => !this.Free.includes(w))
        if (b.length) return null
        return Coords
    }
    BusyCells(active: Layer) {
        const cells: Array<any> = [...this.Zones.values(), ...this.DenyZones.values()].filter((z: Layer) => z != active).map((z: Layer) => [...z.Cells])
        return [].concat(...cells)
    }
    get AllCells() {
        const cells: Array<any> = [...this.FixedZones.values()].map((z: Layer) => [...z.Cells])
        return [].concat(...cells)
    }
    FreeCells(active: Layer) {
        const bc = this.BusyCells(active)
        const AllCells = this.AllCells
        const free = AllCells.filter(w => !bc.includes(w))
        return free
    }
    get ElemCoords() {
        return [...this.Zones.values()].map((z: Layer) => [z.name, z.Dims])
    }
}
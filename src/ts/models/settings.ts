import { Drawer } from "./drawer";
import { isMobile } from './services/ismobile';
export const VIEWPORTTAG = "viewport"
export const SetDimensions = (vp: Element) => {
}
const Mode = () => isMobile() ? 'mobile' : 'desktop'
export const Listen = (Kitchen: Drawer, viewport: Element) => {
    return {
        Listeners: new Map(),
        Events: new Map(
            [
                ['mobile', new Map([
                    ['touchmove', (e: any) => {
                        const [t] = e.touches
                        const active = Kitchen.Active
                        if (!active) return
                        const offset = viewport.getBoundingClientRect()
                        const C = [
                            (t.clientX - (offset.left + active.W / 2)),
                            (t.clientY - (offset.top + active.H / 2))
                        ]
                        const canmove = Kitchen.canPlace(C[0], C[1], active)
                        canmove && active.Place(...canmove)
                    }],
                    ['click', (e: any) => Kitchen.getActive(e.layerX, e.layerY)]
                ])],
                ['desktop', new Map([
                    ['mousemove', (e: any) => {
                        const active = Kitchen.Active
                        if (!active) return
                        const canmove = Kitchen.canPlace(e.layerX - 50, e.layerY - 50, active)
                        canmove && active.Place(...canmove)
                    }],
                    ['click', (e: any) => Kitchen.getActive(e.layerX, e.layerY)]
                ])],
            ]
        ),
        Start: function () {
            const Listeners = this.Events.get(Mode())
            Listeners?.size && Listeners.forEach((listener, event) => viewport.addEventListener(event, listener)
            )
        },
        Stop: function () {
        }
    }
}
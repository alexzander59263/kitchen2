const Elems = [

    {
        name: 'Холодная зона',
        id: 'coldzone',
        color: "cyan",
        size: [110, 110]
    },
    {
        name: 'Зона приготовления',
        id: 'cookzone',
        color: "orange",
        size: [110, 110]

    },
    {
        name: 'Зона мойки',
        id: 'washzone',
        color: "green",
        size: [110, 110]
    },

]

export function* ElemsGenerator() {
    for (const e of Elems) yield { ...e, last: (Elems.indexOf(e) + 1) == Elems.length ? true : false }


}
import { VIEWPORTTAG, SetDimensions, Listen } from './ts/models/settings';
import { Drawer } from './ts/models/drawer';
import { Layer } from './ts/models/layer';
import { ElemsGenerator } from './elems';
import * as M from 'materialize-css';
export const App = () => {
    const [viewport]: any = document.getElementsByTagName(VIEWPORTTAG);
    const NextButton: any = document.getElementById('nxtbut');
    viewport.style.height = viewport.clientWidth * 0.5 + 'px'
    const Kitchen = new Drawer(viewport)
    Kitchen.Fixed = new Layer('k1', viewport, [0, 0, 600, 110, 'grey'])
    Kitchen.Fixed = new Layer('k2', viewport, [0, 110, 110, 168.75, 'grey'])
    Kitchen.Deny = new Layer('denyzone1', viewport, [0, 0, 110, 110, 'red'])
    //Kitchen.Zone = new Layer('hotzone', viewport, [300, 0, 150, 150, 'green'])
    const L = Listen(Kitchen, viewport)
    L.Start()
    const EG = ElemsGenerator()
    const AddElem = () => {
        const { done, value } = EG.next()
        Kitchen.fixAll()
        if (done) ShowResults()
        if (!value) return

        const { id, name, color, size } = value
        const el = new Layer(id, viewport, [120, 0, ...size, color])
        Kitchen.Zone = el
        Kitchen.Activate(el)
        M.Toast.dismissAll();
        M.toast({ html: `<span>Разместите <b>${name}</b></span>`, displayLength: 6000 })
    }
    NextButton.addEventListener('click', AddElem, false)
    const ShowResults = () => {
        console.log('Results: ', Kitchen.ElemCoords);
    }
}
